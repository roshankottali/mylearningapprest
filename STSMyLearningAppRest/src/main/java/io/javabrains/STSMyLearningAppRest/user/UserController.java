package io.javabrains.STSMyLearningAppRest.user;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private UserService service;
	
	/*@RequestMapping("/user/{username}/{passowrd}")
	public String userLogin(@PathVariable String username, @PathVariable String passowrd) {
		return service.userLogin(username,passowrd);
	}*/
	
	@RequestMapping("/users")
	public List<User> getAllUser() {
		return service.getAllUser();
	}

	@RequestMapping("/user/{id}")
	public Optional<User> getUser(@PathVariable int id) {
		return service.getUser(id);
	}
	
	@RequestMapping(method = RequestMethod.POST,value = "/regUser")
	public void addUser(@RequestBody User user) {
		String bCryptPwd = bCryptPasswordEncoder.encode(user.getPassword());
		user.setPassword(bCryptPwd);
		service.addUser(user);
	}
	
	@RequestMapping(method = RequestMethod.PUT,value = "/updateUser")
	public void updateUser(@RequestBody User user) {
		String bCryptPwd = bCryptPasswordEncoder.encode(user.getPassword());
		user.setPassword(bCryptPwd);
		service.updateUser(user);
	}
	
	@RequestMapping(method = RequestMethod.DELETE,value = "/user/{id}")
	public void deleteUser(@PathVariable int id) {
		service.deleteUser(id);
	}
}
