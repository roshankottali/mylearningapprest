package io.javabrains.STSMyLearningAppRest.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	public String userLogin(String usename, String password) {
		if(userRepository.findByUserName(usename) != null) {
			User userData = userRepository.findByUserName(usename);
			if(userData.getPassword().equals(password)) {
				System.out.print("LOGIN SUCCESS");
				return "Login Success";
			} else {
				System.out.print("LOGIN FAILED");
				throw new IllegalArgumentException("INVALID CREDENTIALS");
			}
		} else {
			throw new IllegalArgumentException("INVALID CREDENTIALS");
		}
	}
	
	public List<User> getAllUser() {
		List<User> usersList = new ArrayList<>();
		userRepository.findAll()
		.forEach(usersList::add);
		return usersList;
	}
	
	public Optional<User> getUser(int id) {
		if(userRepository.existsById(id)) {
			return userRepository.findById(id);
		} else {
			throw new IllegalArgumentException("INVALID USER ID");
		}
	}
	
	public void addUser(User user) {
		userRepository.save(user);
	}
	
	public void updateUser(User user) {
		if(userRepository.existsById(user.getId())) {
			userRepository.save(user);
		} else {
			throw new IllegalArgumentException("Invalid user with this ID "+ user.getId() + 
					" exists!");
		}
	}
	
	public void deleteUser(int id) {
		userRepository.deleteById(id);
	}

}
